# Planning

Attempting to make each tag less of a moving target, this will change over time. Doing this in text files instead of Gitlab issues or some other tool to keep these documented in the repo. Good enough for a small personal project.

## App Versions

### 1.5 _Task Improvements_

- tasks edit in dialog
- archive tasks confirm dialog
- don't show task description in app view
- move **Add Task** button to bottom of swimlane
- change showing move buttons to be optional

### Future

- tests

  - reevaluate snapshot testing
  - add swimlane tests

- app

  - filter all tasks (by label, priority, deadline, etc..)
  - see and unarchive archived swimlanes and tasks
  - custom background

- swimlanes

  - sort by (points, creation, labels, alphabetically)
  - focus on, scroll to, new task after clicking new task button

- tasks

  - markdown support in task description field
  - priority
  - story points
  - labels
  - colors
  - deadline
  - checklists
  - link to other tasks (blocked by, subtasks, etc..)
  - status independent of swimlanes? (user defined, could be used for blocked)
  - split task into two tasks
  - date/time task created
  - track date/time task moves to new swimlane
    - could lead to stats! how many tasks with specific label moved to complete this week?

- containerize?

## CI/CD

- Add `build` and `test` stage
- move building and testing out of `pages` job
- run `build` and `test` on all branches
- run all stages on `deployment` branch

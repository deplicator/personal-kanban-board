# Releases

## 0.9 _Proof of Concept_

Complete on commit [2f77e259](https://gitlab.com/deplicator/personal-kanban-board/-/commit/2f77e2591735c0b8ad34c1aaddd86499e3611b98).

Release Notes:

- three swimlanes: waiting, in progress, completed
- task created in waiting, they must have a title (short text field)
- tasks have an optional summary (long text field)
- tasks can be moved from waiting to another swimlane
- tasks can be updated
- tasks can be deleted

<figure>
  <img src="images/interface_0.9.png" width="300px" />
  <figcaption>Interface at tag 0.9</figcaption>
</figure>

## 1.0 _MVP - not pretty but it works_

Complete on commit [ad6f0c64](https://gitlab.com/deplicator/personal-kanban-board/-/commit/4c63551f6d490bb1234d8e6355cbe05c189616f8), no UI changes other than adding export, import, and delete data buttons below swimlanes.

- tasks persist through page reloads by making use of browser's local storage
- tasks can be exported as a json file
- tasks can be imported from a json file (overwrites all current tasks)
- added react tests
- _bug fix:_ multiline description now displays with multiple lines
- _bug fix:_ canceling a task in edit mode will reset contents of text fields

## 1.1 _Add a Menu_

Complete on commit [5a460997](https://gitlab.com/deplicator/personal-kanban-board/-/commit/5a460997faaf82540251e8aaa64886c7eaacf2bb).

- add a simple logo to use as page icon
- add a menu (who would have guessed?)
- add an about dialog with link to gitlab project
- move data buttons to menu
- allow user defined page title, double click the text Personal Kanban Board to edit

<figure>
  <img src="images/interface_1.1.png" width="300px" />
  <figcaption>Interface at tag 1.1</figcaption>
</figure>

### 1.2 _Add Themes_

- add MUI themes, less hard coding of font sizes, weights, colors, etc...
- add five themes: light (MUI default), dark, orange, pink, and green
- add theme selection to storage and data import/export
- swimlanes scroll when tasks pass height of page

<figure>
  <img src="images/interface_1.2_light.png" width="300px" />
  <figcaption>Light</figcaption>
</figure>

<figure>
  <img src="images/interface_1.2_dark.png" width="300px" />
  <figcaption>Dark</figcaption>
</figure>

<figure>
  <img src="images/interface_1.2_orange.png" width="300px" />
  <figcaption>Orange</figcaption>
</figure>

<figure>
  <img src="images/interface_1.2_pink.png" width="300px" />
  <figcaption>Pink</figcaption>
</figure>

<figure>
  <img src="images/interface_1.2_green.png" width="300px" />
  <figcaption>Green</figcaption>
</figure>

### 1.2.1 _Update Themes_

Complete on commit [a775a983](https://gitlab.com/deplicator/personal-kanban-board/-/commit/a775a9832d1d92c5339bd553729b6f799afeda14).

- _bug fix:_ included TextField used when editing app title that was inadvertently left out of theme
- updated tests
- added comments to export variables

### 1.3.0 _Reorder Tasks_

Complete on commit [1db84920](https://gitlab.com/deplicator/personal-kanban-board/-/commit/1db8492072bfe429757c74cabd1f45161b6dc82c).

- Swimlane data stored in redux
- Swimlane keeps track of task order
- _bug fix:_ include board title and theme in data import and export

<figure>
  <img src="images/interface_1.3.0.png" width="300px" />
  <figcaption>Interface at tag 1.3.0</figcaption>
</figure>

### 1.3.1 _Drag and Drop!_

Complete on commit [80b0d795](https://gitlab.com/deplicator/personal-kanban-board/-/commit/80b0d79580bbfa410be318b69e937303af74c9b4).

- drag and drop tasks within swimlane (reordering)
- drag and drop tasks between swimlanes
- update tests

No changes to how the UI is displayed, but now tasks can be clicked and dragged.

### 1.4.0 _User Defined Swimlanes_

Complete on commit [65004282](https://gitlab.com/deplicator/personal-kanban-board/-/commit/650042824e1e31dd8201c38517ff1da7d34bbcba).

- ability to add and edit swimlanes
- rearrange swimlane order with drag and drop
- add swimlane description, show on hover when swimlanes locked
- lock swimlanes (cannot be added, edited, or dragged)
- improved board title editing
  - show buttons when editing
  - can be locked
- change **New Task** to **Add Task**
- replace **delete** text with **archive**
- change delete all data to reset all data

<figure>
  <img src="images/interface_1.4.0.png" width="300px" />
  <figcaption>Interface at tag 1.4.0</figcaption>
</figure>

### 1.4.1 _Improved Data Migration_

Complete on commit [bfbe0b86](https://gitlab.com/deplicator/personal-kanban-board/-/commit/bfbe0b8630afc94331bc6a4d10361f8470b2f8f8).

- data migrates by version stating with 1.3.0
- handles local storage migration and importing data

No UI changes other than an alert that shows on successful migration.

# Personal Kanban Board <img src="docs/images/pbkLogo-32x32.png" width="20px"/>

Client side only [Kanban board](https://en.wikipedia.org/wiki/Kanban_board).

Hosted on Gitlab Pages: [deplicator.gitlab.io/personal-kanban-board](https://deplicator.gitlab.io/personal-kanban-board/)

## Purpose

A personal project, mostly for fun, but also to play with new tech that I don't have a reason to play with otherwise. Maybe to show off that I can:

- build a project with react/typescript from scratch
- iterate on an idea
- document a project (check out [planning](docs/PLANNING.md) and [releases](docs/RELEASES.md) documents)
- create a software release plan
- create and use [CI/CD](https://gitlab.com/deplicator/personal-kanban-board/-/pipelines)

I realize a client side only project has some severe limitations, but I've had [good luck with it before](https://github.com/deplicator/U7PartyPlanner). My hope is that the commit history will prove as interesting as the end result.

## References

No one is an island, I intend to document websites, blogs, tutorials, or videos referenced while building this. Not only to share where the ideas came from, but because it helps future me.

Here is general list of websites referenced while working on this. Specific references will be commented in code.

- [MDN](https://developer.mozilla.org/en-US/)
- [Typescript Handbook](https://www.typescriptlang.org/docs/handbook)
- [Create React App](https://create-react-app.dev/)
- [React](https://reactjs.org/)
- [MUI](https://mui.com/material-ui/)
- [Using Prettier and ESLint](https://khalilstemmler.com/blogs/tooling/prettier/)
- [Redux Toolkit](https://redux-toolkit.js.org/tutorials/quick-start)
- [Redux Testing](https://redux.js.org/usage/writing-tests)
- [This StackOverflow answer was more helpful than Gitlab Pages Docs](https://stackoverflow.com/a/56501501)
- [Git Tagging, referred to this almost every time](https://git-scm.com/book/en/v2/Git-Basics-Tagging)

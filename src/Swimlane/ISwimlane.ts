export interface ISwimlane {
  id: number;
  name: string;
  description: string;
  taskOrder: number[];
  addTaskButton: boolean;

  // depreciated
  newTaskButton?: boolean;
}

import { SetStateAction } from 'react';
import { Button, Dialog, DialogActions, DialogContent } from '@mui/material';

import { useAppDispatch } from '../../storage/hooks';
import { CustomDialogTitle as DialogTitle } from '../../theme/CustomDialogTitle';
import { archiveSwimlane } from '../slice';

const ConfirmArchiveDialog = ({
  isOpen,
  setIsOpen,
  swimlaneId
}: {
  isOpen: boolean;
  setIsOpen: (value: SetStateAction<boolean>) => void;
  swimlaneId: number;
}) => {
  const dispatch = useAppDispatch();

  /** close this dialog */
  const handleClose = () => {
    setIsOpen(false);
  };

  /** remove swimlane and tasks from respective order lists */
  const handleArchive = () => {
    dispatch(archiveSwimlane(swimlaneId));
    setIsOpen(false);
  };

  return (
    <Dialog open={isOpen} onClose={handleClose} fullWidth>
      <DialogTitle>Confirm Delete Swimlane</DialogTitle>
      <DialogContent dividers>
        This action will archive this swimlane and related tasks, there is no
        way to view or recover archived items in this version.
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button variant='contained' color='warning' onClick={handleArchive}>
          Archive
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmArchiveDialog;

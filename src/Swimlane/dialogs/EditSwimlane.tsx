import { SetStateAction, useState } from 'react';
import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  FormControlLabel,
  FormGroup,
  Stack,
  TextField
} from '@mui/material';

import { useAppDispatch } from '../../storage/hooks';
import { CustomDialogTitle as DialogTitle } from '../../theme/CustomDialogTitle';
import { ISwimlane } from '../ISwimlane';
import { appendSwimlane, updateSwimlane } from '../slice';

import ConfirmArchiveDialog from './ConfirmArchive';

const EditSwimlaneDialog = ({
  isOpen,
  setIsOpen,
  swimlane,
  newSwimlane = false
}: {
  isOpen: boolean;
  setIsOpen: (value: SetStateAction<boolean>) => void;
  swimlane: ISwimlane;
  newSwimlane?: boolean;
}) => {
  const { id, name, description, addTaskButton } = swimlane;
  const dispatch = useAppDispatch();

  const [updatedName, setUpdatedName] = useState(name);
  const [updatedDescription, setUpdatedDescription] = useState(description);
  const [updatedAddTaskButton, setUpdatedAddTaskButton] =
    useState(addTaskButton);

  const [confirmDeleteDialog, setConfirmDeleteDialog] = useState(false);

  /** close dialog, reset form fields */
  const handleClose = () => {
    setUpdatedName(name);
    setUpdatedDescription(description);
    setUpdatedAddTaskButton(addTaskButton);
    setIsOpen(false);
  };

  /** update or add new swimlane record */
  const handleSave = () => {
    if (newSwimlane) {
      dispatch(
        appendSwimlane({
          id,
          name: updatedName,
          description: updatedDescription,
          addTaskButton: updatedAddTaskButton
        })
      );
    } else {
      dispatch(
        updateSwimlane({
          ...swimlane,
          name: updatedName,
          description: updatedDescription,
          addTaskButton: updatedAddTaskButton
        })
      );
    }
    setIsOpen(false);
  };

  /** show archive swimlane confirmation dialog */
  const confirmArchive = () => {
    setUpdatedName(swimlane.name);
    setUpdatedDescription(swimlane.description);
    setUpdatedAddTaskButton(swimlane.addTaskButton);
    setIsOpen(false);
    setConfirmDeleteDialog(true);
  };

  return (
    <>
      <Dialog open={isOpen} onClose={handleClose} fullWidth>
        <DialogTitle>
          {newSwimlane ? 'Add Swimlane' : 'Edit Swimlane'}
        </DialogTitle>
        <DialogContent dividers>
          <Stack spacing={1}>
            <TextField
              id='new-swimlane-name'
              label='Name'
              variant='filled'
              fullWidth
              value={updatedName}
              onChange={(event) => setUpdatedName(event.target.value)}
              autoFocus
            />
            <TextField
              id='new-swimlane-description'
              label='Description'
              multiline
              rows={4}
              variant='filled'
              fullWidth
              value={updatedDescription}
              onChange={(event) => setUpdatedDescription(event.target.value)}
            />
            <FormGroup>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={updatedAddTaskButton}
                    onChange={(event) => {
                      setUpdatedAddTaskButton(event.target.checked);
                    }}
                  />
                }
                label='Show New Task button'
              />
            </FormGroup>
          </Stack>
        </DialogContent>
        <DialogActions
          sx={{ justifyContent: newSwimlane ? 'flex-end' : 'space-between' }}
        >
          {!newSwimlane && (
            <Button
              variant='contained'
              color='warning'
              onClick={confirmArchive}
            >
              Archive
            </Button>
          )}
          <Stack direction='row' spacing={1}>
            <Button onClick={handleClose}>Cancel</Button>
            <Button
              variant='contained'
              disabled={updatedName === ''}
              onClick={handleSave}
            >
              {newSwimlane ? 'Add' : 'Save'}
            </Button>
          </Stack>
        </DialogActions>
      </Dialog>

      <ConfirmArchiveDialog
        isOpen={confirmDeleteDialog}
        setIsOpen={setConfirmDeleteDialog}
        swimlaneId={swimlane.id}
      />
    </>
  );
};

export default EditSwimlaneDialog;

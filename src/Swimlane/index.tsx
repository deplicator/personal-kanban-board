import { useEffect, useState } from 'react';
import { Draggable } from 'react-beautiful-dnd';
import {
  Box,
  Button,
  Paper,
  Toolbar,
  Tooltip,
  Typography
} from '@mui/material';
import { AddCircleOutline } from '@mui/icons-material';

import { selectApp } from '../App/slice';
import { useAppDispatch, useAppSelector } from '../storage/hooks';
import Task from '../Task';
import { ITask } from '../Task/ITask';
import { selectTask } from '../Task/slice';
import { StrictModeDroppable as Droppable } from '../utilities/StrictModeDroppable';

import EditSwimlaneDialog from './dialogs/EditSwimlane';
import { ISwimlane } from './ISwimlane';
import { appendTask } from './slice';

/** Swimlane - column for tasks */
const Swimlane = ({
  swimlane,
  swimlanePosition
}: {
  swimlane: ISwimlane;
  swimlanePosition: { index: number; last: boolean };
}) => {
  const dispatch = useAppDispatch();

  const { id, name, description, taskOrder, addTaskButton = false } = swimlane;

  const [addingTask, setAddingTask] = useState(false);
  const [addedTaskId, setAddedTaskId] = useState(-1);
  const [editSwimlaneDialog, setEditSwimlaneDialog] = useState(false);

  const {
    locks: { swimlanes: swimlanesLocked }
  } = useAppSelector(selectApp);
  const { taskList, nextTaskId } = useAppSelector(selectTask);

  const swimlaneTasks = taskOrder.map(
    (taskId) => taskList.find(({ id }) => id === taskId) as ITask
  );

  /** set addingTask flag true */
  const showAddTaskElement = () => {
    setAddingTask(true);
  };

  /** open swimlane dialog if not locked */
  const editSwimlane = () => {
    if (!swimlanesLocked) {
      setEditSwimlaneDialog(true);
    }
  };

  // update swimlane record with new task id on successful creation
  useEffect(() => {
    if (addedTaskId > -1) {
      dispatch(appendTask({ id, addedTask: addedTaskId }));
      setAddedTaskId(-1);
    }
  }, [addingTask]);

  return (
    <>
      <Draggable
        draggableId={`swimlane-${id}`}
        index={swimlanePosition.index}
        isDragDisabled={swimlanesLocked}
      >
        {(draggableProvided) => (
          <Paper
            data-testid={`swimlane-${id}`}
            ref={draggableProvided.innerRef}
            {...draggableProvided.draggableProps}
            sx={{
              p: 1,
              minHeight: '85vh',
              width: 350,
              minWidth: 250,
              overflowY: 'auto'
            }}
          >
            <Toolbar
              variant='dense'
              disableGutters
              onDoubleClick={editSwimlane}
              sx={{ px: 1 }}
              {...draggableProvided.dragHandleProps}
            >
              <Tooltip
                followCursor
                enterDelay={750}
                enterNextDelay={750}
                title={swimlanesLocked ? description : ''}
              >
                <Typography variant='h2' sx={{ flexGrow: 1 }}>
                  {name}
                </Typography>
              </Tooltip>
              {addTaskButton && (
                <Button
                  aria-label='add-task'
                  size='small'
                  startIcon={<AddCircleOutline />}
                  onClick={showAddTaskElement}
                >
                  Add Task
                </Button>
              )}
            </Toolbar>
            <Droppable droppableId={`swimlane-${id}`} type='Task'>
              {(droppableProvided) => (
                <Box
                  height='calc(100% - 48px - 16px)' // swimlane header and padding
                  ref={droppableProvided.innerRef}
                  {...droppableProvided.droppableProps}
                >
                  {swimlaneTasks.map((task, index) => (
                    <Task
                      key={`task-${task.id}`}
                      task={task}
                      taskPosition={{
                        index: index,
                        last: index === swimlaneTasks.length - 1
                      }}
                      swimlaneId={id}
                    />
                  ))}

                  {addingTask && (
                    <Task
                      task={{
                        id: nextTaskId,
                        title: '',
                        description: ''
                      }}
                      taskPosition={{
                        index: swimlaneTasks.length,
                        last: true
                      }}
                      swimlaneId={id}
                      setAddingTask={setAddingTask}
                      setAddedTask={setAddedTaskId}
                    />
                  )}
                  {droppableProvided.placeholder}
                </Box>
              )}
            </Droppable>
          </Paper>
        )}
      </Draggable>

      <EditSwimlaneDialog
        isOpen={editSwimlaneDialog}
        setIsOpen={setEditSwimlaneDialog}
        swimlane={swimlane}
      />
    </>
  );
};

export default Swimlane;

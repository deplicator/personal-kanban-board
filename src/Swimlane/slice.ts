import { createSlice } from '@reduxjs/toolkit';

import type { RootState } from '../storage/store';

import { ISwimlane } from './ISwimlane';

/** Initial state of swimlanes */
export const initialState: {
  swimlaneList: ISwimlane[];
  swimlaneOrder: number[];
  nextSwimlaneId: number;
} = {
  swimlaneList: [
    {
      id: 0,
      name: 'Waiting',
      description: 'Tasks that have not been started.',
      taskOrder: [],
      addTaskButton: true
    },
    {
      id: 1,
      name: 'In Progress',
      description: 'Tasks that are in work.',
      taskOrder: [],
      addTaskButton: false
    },
    {
      id: 2,
      name: 'Complete',
      description: 'Tasks that are finished.',
      taskOrder: [],
      addTaskButton: false
    }
  ],
  swimlaneOrder: [0, 1, 2],
  nextSwimlaneId: 3
};

const swimlaneSlice = createSlice({
  name: 'swimlanes',
  initialState,
  reducers: {
    appendTask: (state, { payload }) => {
      const swimlaneIndex = state.swimlaneList.findIndex(
        ({ id }) => id === payload.id
      );
      return {
        ...state,
        swimlaneList: [
          ...state.swimlaneList.slice(0, swimlaneIndex),
          {
            ...state.swimlaneList[swimlaneIndex],
            taskOrder: [
              ...state.swimlaneList[swimlaneIndex].taskOrder,
              payload.addedTask
            ]
          },
          ...state.swimlaneList.slice(swimlaneIndex + 1)
        ]
      };
    },
    moveTask: (
      state,
      { payload: { taskId, newTaskIndex, fromSwimlaneId, toSwimlaneId } }
    ) => {
      // reorder within same list
      if (toSwimlaneId === undefined || fromSwimlaneId === toSwimlaneId) {
        const taskOrderWithoutTaskId = state.swimlaneList[
          fromSwimlaneId
        ].taskOrder.filter((id) => id !== taskId);

        const updatedTaskOrder = [
          ...taskOrderWithoutTaskId.slice(0, newTaskIndex),
          taskId,
          ...taskOrderWithoutTaskId.slice(newTaskIndex)
        ];

        return {
          ...state,
          swimlaneList: state.swimlaneList.map((swimlane, index) =>
            index === fromSwimlaneId
              ? {
                  ...swimlane,
                  taskOrder: updatedTaskOrder
                }
              : swimlane
          )
        };
      }

      // move to new list
      if (newTaskIndex === undefined) {
        newTaskIndex = state.swimlaneList[toSwimlaneId].taskOrder.length;
      }

      const updatedFromTaskOrder = state.swimlaneList[
        fromSwimlaneId
      ].taskOrder.filter((id) => id !== taskId);

      const updatedToTaskOrder = [
        ...state.swimlaneList[toSwimlaneId].taskOrder.slice(0, newTaskIndex),
        taskId,
        ...state.swimlaneList[toSwimlaneId].taskOrder.slice(newTaskIndex)
      ];

      return {
        ...state,
        swimlaneList: state.swimlaneList.map((swimlane, index) =>
          index === fromSwimlaneId
            ? { ...swimlane, taskOrder: updatedFromTaskOrder }
            : index === toSwimlaneId
            ? { ...swimlane, taskOrder: updatedToTaskOrder }
            : swimlane
        )
      };
    },
    archiveTask: (state, { payload: { taskId, swimlaneId } }) => {
      // - Removes taskId from swimlane taskOrder (archiving the task)

      return {
        ...state,
        swimlaneList: state.swimlaneList.map((swimlane) =>
          swimlane.id === swimlaneId
            ? {
                ...swimlane,
                taskOrder: swimlane.taskOrder.filter((id) => id !== taskId)
              }
            : swimlane
        )
      };
    },
    updateSwimlane: (state, { payload }) => {
      const swimlaneIndex = state.swimlaneList.findIndex(
        ({ id }) => id === payload.id
      );
      return {
        ...state,
        swimlaneList: [
          ...state.swimlaneList.slice(0, swimlaneIndex),
          payload,
          ...state.swimlaneList.slice(swimlaneIndex + 1)
        ]
      };
    },
    appendSwimlane: (
      state,
      { payload: { id, name, description, addTaskButton } }
    ) => ({
      ...state,
      swimlaneList: [
        ...state.swimlaneList,
        {
          id,
          name,
          description,
          taskOrder: [],
          addTaskButton
        }
      ],
      swimlaneOrder: [...state.swimlaneOrder, id],
      nextSwimlaneId: ++id
    }),
    moveSwimlane: (state, { payload: { swimlaneId, newSwimlaneIndex } }) => {
      const swimlaneOrderWithoutSwimlaneId = state.swimlaneOrder.filter(
        (id) => id !== swimlaneId
      );

      return {
        ...state,
        swimlaneOrder: [
          ...swimlaneOrderWithoutSwimlaneId.slice(0, newSwimlaneIndex),
          swimlaneId,
          ...swimlaneOrderWithoutSwimlaneId.slice(newSwimlaneIndex)
        ]
      };
    },
    archiveSwimlane: (state, { payload }) => {
      // - Payload is swimlaneId
      // - Removes swimlane id from swimlaneOrder (archiving the swimlane)
      // - Does not remove swimlane from swimlaneList
      // - Clears swimlane's taskOrder (archiving the tasks)
      const swimlaneIndex = state.swimlaneList.findIndex(
        ({ id }) => id === payload
      );

      const UpdateArchivedSwimlane = {
        ...state.swimlaneList[swimlaneIndex],
        taskOrder: []
      };

      return {
        ...state,
        swimlaneList: [
          ...state.swimlaneList.slice(0, swimlaneIndex),
          UpdateArchivedSwimlane,
          ...state.swimlaneList.slice(swimlaneIndex + 1)
        ],
        swimlaneOrder: state.swimlaneOrder.filter((id) => id !== payload)
      };
    },
    reset: (state, { payload }) => (payload ? payload : initialState)
  }
});

export const {
  appendTask,
  moveTask,
  archiveTask,
  updateSwimlane,
  appendSwimlane,
  moveSwimlane,
  archiveSwimlane,
  reset
} = swimlaneSlice.actions;

export const selectSwimlane = (state: RootState) => state.swimlanes;

export default swimlaneSlice.reducer;

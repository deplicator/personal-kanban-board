import { Dispatch, SetStateAction, useState } from 'react';
import { Draggable } from 'react-beautiful-dnd';
import {
  alpha,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  IconButton,
  Paper,
  Stack,
  TextField,
  Typography
} from '@mui/material';
import {
  ArrowDropDown,
  ArrowDropUp,
  ArrowLeft,
  ArrowRight
} from '@mui/icons-material';

import { useAppDispatch, useAppSelector } from '../storage/hooks';
import { archiveTask, moveTask, selectSwimlane } from '../Swimlane/slice';

import { ITask } from './ITask';
import { addTask, updateTask } from './slice';

enum OrderDirection {
  Up = -1,
  Down = 1
}

enum MoveDirection {
  Backward = -1,
  Forward = 1
}

/** Task Card - show or edit task details */
const Task = ({
  task,
  taskPosition,
  swimlaneId,
  setAddingTask,
  setAddedTask
}: {
  task: ITask;
  taskPosition: { index: number; last: boolean };
  swimlaneId: number;
  setAddingTask?: Dispatch<SetStateAction<boolean>>; // defined when adding new task
  setAddedTask?: Dispatch<SetStateAction<number>>;
}) => {
  const dispatch = useAppDispatch();

  const { id, title, description } = task;

  const [isEditing, setIsEditing] = useState(setAddingTask ? true : false);
  const [taskTitle, setTaskTitle] = useState(title);
  const [taskDescription, setTaskDescription] = useState(description);

  const { swimlaneOrder } = useAppSelector(selectSwimlane);
  const swimlaneFirst = swimlaneOrder.indexOf(swimlaneId) === 0;
  const swimlaneLast =
    swimlaneOrder.indexOf(swimlaneId) === swimlaneOrder.length - 1;

  /** put task into editing mode */
  const editTask = () => {
    setIsEditing(true);
  };

  /** take task out of editing mode, clear edits */
  const cancelTask = () => {
    setTaskTitle(title);
    setTaskDescription(description);
    setIsEditing(false);
    setAddingTask && setAddingTask(false);
  };

  /** add new task record, cancel edit mode */
  const createTask = () => {
    dispatch(
      addTask({
        id,
        title: taskTitle,
        description: taskDescription
      })
    );
    setAddingTask && setAddingTask(false);
    setAddedTask && setAddedTask(id);
    setIsEditing(false);
  };

  /** save changes to task record, cancel edit mode */
  const saveTask = () => {
    dispatch(
      updateTask({
        id,
        title: taskTitle,
        description: taskDescription
      })
    );
    setIsEditing(false);
  };

  /** archive task record, cancel edit mode */
  const deleteTask = () => {
    dispatch(
      archiveTask({
        taskId: id,
        swimlaneId: swimlaneId
      })
    );
    setIsEditing(false);
  };

  /** change order (up or down) of task in swimlane by 1 */
  const orderTasks = (direction: OrderDirection) => {
    const newTaskIndex = taskPosition.index + direction;
    dispatch(
      moveTask({
        taskId: id,
        newTaskIndex,
        fromSwimlaneId: swimlaneId
      })
    );
  };

  /** move task (forward or back) to another swimlane by 1 */
  const changeSwimlane = (direction: MoveDirection) => {
    const fromSwimlaneIndex = swimlaneOrder.indexOf(swimlaneId);

    const toSwimlaneId =
      fromSwimlaneIndex !== -1
        ? swimlaneOrder[fromSwimlaneIndex + direction]
        : 0;

    dispatch(
      moveTask({
        taskId: id,
        fromSwimlaneId: swimlaneId,
        toSwimlaneId
      })
    );
  };

  const cardContents = isEditing ? (
    <CardContent sx={{ p: 0, mb: 1 }}>
      <Stack spacing={1}>
        <TextField
          id='new-task-title'
          label='Title'
          variant='filled'
          fullWidth
          value={taskTitle}
          onChange={(event) => setTaskTitle(event.target.value)}
          autoFocus
        />
        <TextField
          id='new-task-description'
          label='Description'
          multiline
          rows={4}
          variant='filled'
          fullWidth
          value={taskDescription}
          onChange={(event) => setTaskDescription(event.target.value)}
        />
      </Stack>
    </CardContent>
  ) : (
    <CardContent sx={{ p: 0, ':last-child': { p: 0 } }}>
      <CardHeader
        disableTypography
        title={<Typography variant='h3'>{title}</Typography>}
        action={
          <Button
            aria-label='edit-task'
            size='small'
            variant='text'
            onClick={editTask}
          >
            Edit
          </Button>
        }
        sx={{
          p: 0,
          pb: 1,
          '& .MuiCardHeader-action': { m: 0 }
        }}
      />
      {description && (
        <Paper
          variant='outlined'
          sx={{
            p: 1,
            backgroundColor: (theme) => alpha(theme.palette.primary.main, 0.1),
            whiteSpace: 'pre-line'
          }}
        >
          {description}
        </Paper>
      )}
    </CardContent>
  );

  const cardActions = isEditing ? (
    <CardActions
      sx={{
        justifyContent: setAddingTask ? 'flex-end' : 'space-between'
      }}
    >
      {!setAddingTask && (
        <Button
          aria-label='archive-task'
          size='small'
          color='warning'
          onClick={deleteTask}
        >
          Archive
        </Button>
      )}
      <Stack direction='row' spacing={1}>
        <Button aria-label='cancel-task' size='small' onClick={cancelTask}>
          Cancel
        </Button>
        <Button
          aria-label='save-task'
          variant='contained'
          size='small'
          onClick={setAddingTask ? createTask : saveTask}
          disabled={taskTitle === ''}
        >
          Save
        </Button>
      </Stack>
    </CardActions>
  ) : (
    <CardActions sx={{ p: 0, justifyContent: 'space-between' }}>
      <Stack direction='row'>
        <IconButton
          aria-label='move-up'
          size='small'
          color='primary'
          onClick={() => orderTasks(OrderDirection.Up)}
          sx={{
            visibility: taskPosition.index === 0 ? 'hidden' : undefined
          }}
        >
          <ArrowDropUp />
        </IconButton>
        {!taskPosition.last && (
          <IconButton
            aria-label='move-down'
            size='small'
            color='primary'
            onClick={() => orderTasks(OrderDirection.Down)}
          >
            <ArrowDropDown />
          </IconButton>
        )}
      </Stack>
      <Stack direction='row'>
        {!swimlaneFirst && (
          <IconButton
            aria-label='move-back'
            size='small'
            color='primary'
            onClick={() => changeSwimlane(MoveDirection.Backward)}
          >
            <ArrowLeft />
          </IconButton>
        )}
        {!swimlaneLast && (
          <IconButton
            aria-label='move-forward'
            size='small'
            color='primary'
            onClick={() => changeSwimlane(MoveDirection.Forward)}
          >
            <ArrowRight />
          </IconButton>
        )}
      </Stack>
    </CardActions>
  );

  return (
    <Draggable
      draggableId={`task-${id}`}
      index={taskPosition.index}
      isDragDisabled={isEditing}
    >
      {(draggableProvided) => (
        <Card
          data-testid={`task-${id}`}
          ref={draggableProvided.innerRef}
          {...draggableProvided.draggableProps}
          {...draggableProvided.dragHandleProps}
          sx={{
            width: '100%',
            mb: 1,
            p: 1,
            backgroundColor: isEditing
              ? (theme) => alpha(theme.palette.secondary.dark, 0.2)
              : (theme) => alpha(theme.palette.primary.main, 0.05)
          }}
        >
          {cardContents}
          {cardActions}
        </Card>
      )}
    </Draggable>
  );
};

export default Task;

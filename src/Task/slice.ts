import { createSlice } from '@reduxjs/toolkit';

import type { RootState } from '../storage/store';

import { ITask } from './ITask';

/** Initial state of tasks */
export const initialState = {
  taskList: [] as ITask[],
  nextTaskId: 0
};

const taskSlice = createSlice({
  name: 'tasks',
  initialState,
  reducers: {
    addTask: (state, { payload }) => ({
      // payload is ITask
      taskList: [...state.taskList, payload],
      nextTaskId: payload.id + 1
    }),
    updateTask: (state, { payload }) => {
      // payload is ITask
      const taskIndex = state.taskList.findIndex(({ id }) => id === payload.id);

      return {
        ...state,
        taskList: [
          ...state.taskList.slice(0, taskIndex),
          payload,
          ...state.taskList.slice(taskIndex + 1)
        ]
      };
    },
    reset: (state, { payload }) => (payload ? payload : initialState)
  }
});

export const { addTask, updateTask, reset } = taskSlice.actions;

export const selectTask = (state: RootState) => state.tasks;

export default taskSlice.reducer;

import { useState } from 'react';
import { DragDropContext, DropResult } from 'react-beautiful-dnd';
import { Box, Button, CssBaseline, Stack, ThemeProvider } from '@mui/material';
import { PlaylistAddOutlined } from '@mui/icons-material';

import { useAppDispatch, useAppSelector } from '../storage/hooks';
import Swimlane from '../Swimlane';
import EditSwimlaneDialog from '../Swimlane/dialogs/EditSwimlane';
import { ISwimlane } from '../Swimlane/ISwimlane';
import { moveSwimlane, moveTask, selectSwimlane } from '../Swimlane/slice';
import { themeMapper } from '../theme/utilities';
import { StrictModeDroppable as Droppable } from '../utilities/StrictModeDroppable';

import { selectApp } from './slice';
import TopBar from './TopBar';

const App = () => {
  const dispatch = useAppDispatch();

  const [editSwimlaneDialog, setEditSwimlaneDialog] = useState(false);

  const {
    themeName,
    locks: { swimlanes: swimlanesLocked }
  } = useAppSelector(selectApp);
  const { swimlaneList, swimlaneOrder, nextSwimlaneId } =
    useAppSelector(selectSwimlane);

  const orderedSwimlanes = swimlaneOrder.map(
    (swimlaneId) =>
      swimlaneList.find(({ id }) => id === swimlaneId) as ISwimlane
  );

  /** open edit swimlane dialog */
  const addSwimlane = () => {
    setEditSwimlaneDialog(true);
  };

  const handleDragEnd = (result: DropResult) => {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    if (result.type === 'Swimlane') {
      const swimlaneId = parseInt(result.draggableId.split('-')[1], 10);

      dispatch(
        moveSwimlane({
          swimlaneId,
          newSwimlaneIndex: result.destination.index
        })
      );
    }

    if (result.type === 'Task') {
      const taskId = parseInt(result.draggableId.split('-')[1], 10);
      const fromSwimlaneId = parseInt(
        result.source.droppableId.split('-')[1],
        10
      );
      const toSwimlaneId = parseInt(
        result.destination.droppableId.split('-')[1],
        10
      );

      dispatch(
        moveTask({
          taskId,
          newTaskIndex: result.destination.index,
          fromSwimlaneId,
          toSwimlaneId
        })
      );
    }
  };

  return (
    <ThemeProvider theme={themeMapper(themeName)}>
      <CssBaseline />
      <TopBar />
      <Box
        display='flex'
        alignItems='flex-start'
        padding={2}
        sx={{ overflowX: 'scroll' }}
      >
        <DragDropContext onDragEnd={handleDragEnd}>
          <Droppable droppableId='app' direction='horizontal' type='Swimlane'>
            {(droppableProvided) => (
              <Stack
                direction='row'
                spacing={2}
                justifyContent='flex-start'
                sx={{ height: 'calc(100vh - 98px - 16px)' }} // TopBar height + border + stack margin + scroll
                ref={droppableProvided.innerRef}
                {...droppableProvided.droppableProps}
              >
                {orderedSwimlanes.map((swimlane, index) => (
                  <Swimlane
                    key={`swimlane-${swimlane.id}`}
                    swimlane={swimlane}
                    swimlanePosition={{
                      index,
                      last: index === swimlaneList.length - 1
                    }}
                  />
                ))}
                {droppableProvided.placeholder}
              </Stack>
            )}
          </Droppable>
        </DragDropContext>
        {!swimlanesLocked && (
          <Button
            aria-label='add-swimlane'
            startIcon={<PlaylistAddOutlined />}
            onClick={addSwimlane}
            sx={{ ml: 2, minWidth: '170px' }}
          >
            Add Swimlane
          </Button>
        )}
      </Box>

      <EditSwimlaneDialog
        isOpen={editSwimlaneDialog}
        setIsOpen={setEditSwimlaneDialog}
        swimlane={{
          id: nextSwimlaneId,
          name: '',
          description: '',
          taskOrder: [],
          addTaskButton: false
        }}
        newSwimlane={true}
      />
    </ThemeProvider>
  );
};

export default App;

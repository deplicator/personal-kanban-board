import { createSlice } from '@reduxjs/toolkit';

import type { RootState } from '../storage/store';
import { ThemeNames } from '../theme/utilities';

/** Initial state of app options */
export const initialState: {
  boardTitle: string;
  version: string;
  themeName: ThemeNames;
  locks: {
    boardTitle: boolean;
    swimlanes: boolean;
    tasks: boolean;
  };
} = {
  boardTitle: 'Personal Kanban Board',
  version: '1.4.0',
  themeName: 'light',
  locks: {
    boardTitle: false,
    swimlanes: false,
    tasks: false
  }
};

const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    updateBoardTitle: (state, { payload }) => ({
      ...state,
      boardTitle: payload
    }),
    updateTheme: (state, { payload }) => ({ ...state, themeName: payload }),
    updateLocks: (state, { payload: { name, locked } }) => ({
      ...state,
      locks: {
        ...state.locks,
        [Object.keys(state.locks).find((key) => key === name) as string]: locked
      }
    }),
    reset: (state, { payload }) => (payload ? payload : initialState)
  }
});

export const { updateBoardTitle, updateTheme, updateLocks, reset } =
  appSlice.actions;

export const selectApp = (state: RootState) => state.app;

export default appSlice.reducer;

import { useState } from 'react';
import {
  Divider,
  Icon,
  IconButton,
  Menu,
  MenuItem,
  Typography
} from '@mui/material';
import {
  Circle,
  DarkModeOutlined,
  DeleteSweepOutlined,
  FileDownloadOutlined,
  FileUploadOutlined,
  InfoOutlined,
  LightModeOutlined,
  LockOpenOutlined,
  LockOutlined,
  Menu as MenuIcon
} from '@mui/icons-material';

import gitlabLogo from '../../assets/gitlab-logo-500.png';
import { useAppDispatch, useAppSelector } from '../../storage/hooks';
import {
  greenTheme,
  lightTheme,
  orangeTheme,
  pinkTheme
} from '../../theme/theme';
import { ThemeNames } from '../../theme/utilities';
import AboutDialog from '../dialogs/About';
import ImportDataDialog from '../dialogs/ImportData';
import ResetDataDialog from '../dialogs/ResetData';
import { selectApp, updateLocks, updateTheme } from '../slice';

const AppMenu = () => {
  const dispatch = useAppDispatch();

  const { themeName, locks } = useAppSelector(selectApp);

  const [menuAnchor, setMenuAnchor] = useState<HTMLElement | null>(null);
  const [aboutDialog, setAboutDialog] = useState(false);
  const [importDataDialog, setImportDataDialog] = useState(false);
  const [deleteDataDialog, setDeleteDataDialog] = useState(false);

  /** show menu options */
  const openMenu = (event: React.MouseEvent<HTMLElement>) => {
    setMenuAnchor(event.currentTarget);
  };

  /** hide menu options */
  const closeMenu = () => {
    setMenuAnchor(null);
  };

  /** enable or disable editing board title */
  const toggleTitleLock = () => {
    dispatch(updateLocks({ name: 'boardTitle', locked: !locks.boardTitle }));
    setMenuAnchor(null);
  };

  /** enable or disable adding, editing, and dragging swimlanes */
  const toggleSwimlaneLock = () => {
    dispatch(updateLocks({ name: 'swimlanes', locked: !locks.swimlanes }));
    setMenuAnchor(null);
  };

  /** save theme change to storage */
  const handleThemeChange = (newTheme: ThemeNames) => {
    dispatch(updateTheme(newTheme));
    setMenuAnchor(null);
  };

  /** show about dialog */
  const openAboutDialog = () => {
    setAboutDialog(true);
    setMenuAnchor(null);
  };

  /** show import data dialog */
  const openImportDialog = () => {
    setImportDataDialog(true);
    setMenuAnchor(null);
  };

  /** show delete data dialog */
  const openDeleteDialog = () => {
    setDeleteDataDialog(true);
    setMenuAnchor(null);
  };

  /** download local storage as json file */
  const exportData = () => {
    const lsString = JSON.stringify(localStorage['state']);
    const lsBlob = new Blob([lsString], { type: 'text/plain' });
    const link = document.createElement('a');
    link.href = window.URL.createObjectURL(lsBlob);
    link.setAttribute('download', 'pkbData.json');
    link.click();
    setMenuAnchor(null);
  };

  return (
    <>
      <IconButton
        aria-label='settings-menu'
        size='large'
        color='inherit'
        onClick={openMenu}
      >
        <MenuIcon />
      </IconButton>

      <Menu
        anchorEl={menuAnchor}
        open={Boolean(menuAnchor)}
        onClose={closeMenu}
      >
        <Typography align='center' variant='body2'>
          Locks
        </Typography>
        <MenuItem aria-label='lock-swimlanes' onClick={toggleTitleLock}>
          {locks?.boardTitle ? (
            <LockOutlined
              sx={{ mr: 1, color: lightTheme.palette.primary.main }}
            />
          ) : (
            <LockOpenOutlined
              sx={{ mr: 1, color: lightTheme.palette.primary.main }}
            />
          )}
          Board Title
        </MenuItem>
        <MenuItem aria-label='lock-swimlanes' onClick={toggleSwimlaneLock}>
          {locks?.swimlanes ? (
            <LockOutlined
              sx={{ mr: 1, color: lightTheme.palette.primary.main }}
            />
          ) : (
            <LockOpenOutlined
              sx={{ mr: 1, color: lightTheme.palette.primary.main }}
            />
          )}
          Swimlanes
        </MenuItem>

        <Divider />
        <Typography align='center' variant='body2'>
          Themes
        </Typography>
        <MenuItem
          selected={'light' === themeName}
          onClick={() => handleThemeChange('light')}
        >
          <LightModeOutlined
            sx={{ mr: 1, color: lightTheme.palette.primary.main }}
          />
          Light
        </MenuItem>
        <MenuItem
          selected={'dark' === themeName}
          onClick={() => handleThemeChange('dark')}
        >
          <DarkModeOutlined sx={{ mr: 1 }} />
          Dark
        </MenuItem>
        <MenuItem
          selected={'orange' === themeName}
          onClick={() => handleThemeChange('orange')}
        >
          <Circle sx={{ mr: 1, color: orangeTheme.palette.primary.main }} />
          Orange
        </MenuItem>
        <MenuItem
          selected={'pink' === themeName}
          onClick={() => handleThemeChange('pink')}
        >
          <Circle sx={{ mr: 1, color: pinkTheme.palette.primary.main }} />
          Pink
        </MenuItem>
        <MenuItem
          selected={'green' === themeName}
          onClick={() => handleThemeChange('green')}
        >
          <Circle sx={{ mr: 1, color: greenTheme.palette.primary.main }} />
          Green
        </MenuItem>
        <Divider />

        <Typography align='center' variant='body2'>
          Data
        </Typography>
        <MenuItem onClick={exportData}>
          <FileDownloadOutlined sx={{ mr: 1 }} />
          Export
        </MenuItem>
        <MenuItem onClick={openImportDialog}>
          <FileUploadOutlined sx={{ mr: 1 }} />
          Import
        </MenuItem>
        <MenuItem onClick={openDeleteDialog}>
          <DeleteSweepOutlined sx={{ mr: 1 }} />
          Reset
        </MenuItem>
        <Divider />

        <Typography align='center' variant='body2'>
          Info
        </Typography>
        <MenuItem onClick={openAboutDialog}>
          <InfoOutlined sx={{ mr: 1 }} />
          About
        </MenuItem>
        <MenuItem
          component='a'
          href='https://gitlab.com/deplicator/personal-kanban-board'
        >
          <Icon sx={{ ml: '-5px', mr: 1 }}>
            <img src={gitlabLogo} height={32} width={32} />
          </Icon>
          Source
        </MenuItem>
      </Menu>

      <AboutDialog isOpen={aboutDialog} setIsOpen={setAboutDialog} />

      <ImportDataDialog
        isOpen={importDataDialog}
        setIsOpen={setImportDataDialog}
      />

      <ResetDataDialog
        isOpen={deleteDataDialog}
        setIsOpen={setDeleteDataDialog}
      />
    </>
  );
};

export default AppMenu;

import { useState } from 'react';
import { Button, IconButton, Stack, Typography } from '@mui/material';
import { Check, Close, ModeEditOutlined } from '@mui/icons-material';

import { useAppDispatch, useAppSelector } from '../../storage/hooks';
import { CustomTextField } from '../../theme/CustomTextField';
import { selectApp, updateBoardTitle } from '../slice';

const BoardTitle = () => {
  const dispatch = useAppDispatch();

  const { boardTitle } = useAppSelector(selectApp);
  const {
    locks: { boardTitle: titleLocked }
  } = useAppSelector(selectApp);

  const [editTitle, setEditTitle] = useState(false);
  const [newTitle, setNewTitle] = useState(boardTitle);

  const cancelTitleEdit = () => {
    setNewTitle(boardTitle);
    setEditTitle(false);
  };

  const saveTitleEdit = () => {
    if (newTitle === '') {
      return;
    }
    dispatch(updateBoardTitle(newTitle));
    setEditTitle(false);
  };

  /** escape doesn't save title change, enter does */
  const handleKeyDown = (event: { key: string }) => {
    if (event.key === 'Escape') {
      cancelTitleEdit();
    }
    if (event.key === 'Enter') {
      saveTitleEdit();
    }
  };

  return (
    <Stack direction='row' spacing={2} alignItems='center'>
      {editTitle ? (
        <>
          <CustomTextField
            autoFocus
            value={newTitle}
            onKeyDown={handleKeyDown}
            onChange={(e) => setNewTitle(e.target.value)}
          />
          <Button
            aria-label='edit-board-title'
            color='inherit'
            onClick={() => cancelTitleEdit()}
          >
            <Close />
          </Button>
          <Button
            aria-label='edit-board-title'
            color='secondary'
            variant='contained'
            onClick={() => saveTitleEdit()}
          >
            <Check />
          </Button>
        </>
      ) : (
        <Typography variant='h1' sx={{ pl: 1 }}>
          {boardTitle}
        </Typography>
      )}
      {!titleLocked && !editTitle && (
        <IconButton
          aria-label='edit-board-title'
          color='inherit'
          onClick={() => setEditTitle(true)}
        >
          <ModeEditOutlined />
        </IconButton>
      )}
    </Stack>
  );
};

export default BoardTitle;

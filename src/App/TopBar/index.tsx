import { AppBar, Toolbar } from '@mui/material';

import AppMenu from './AppMenu';
import BoardTitle from './BoardTitle';

const TopBar = () => {
  return (
    <AppBar position='static'>
      <Toolbar>
        <AppMenu />
        <BoardTitle />
      </Toolbar>
    </AppBar>
  );
};

export default TopBar;

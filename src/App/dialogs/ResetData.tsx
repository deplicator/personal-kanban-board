import { SetStateAction } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText
} from '@mui/material';

import { useAppDispatch } from '../../storage/hooks';
import { saveState } from '../../storage/localStorage';
import {
  initialState as initialSwimlaneState,
  reset as resetSwimlanes
} from '../../Swimlane/slice';
import {
  initialState as initialTaskState,
  reset as resetTasks
} from '../../Task/slice';
import { CustomDialogTitle as DialogTitle } from '../../theme/CustomDialogTitle';
import { initialState as initialAppState, reset as resetApp } from '../slice';

/** Confirmation and warning to delete data. */
const ResetDataDialog = ({
  isOpen,
  setIsOpen
}: {
  isOpen: boolean;
  setIsOpen: (value: SetStateAction<boolean>) => void;
}) => {
  const dispatch = useAppDispatch();

  const handleClose = () => {
    setIsOpen(false);
  };

  /** re-initialize redux and local storage state */
  const clearLocalStorage = () => {
    saveState({
      app: initialAppState,
      swimlanes: initialSwimlaneState,
      tasks: initialTaskState
    });
    dispatch(resetApp(initialAppState));
    dispatch(resetSwimlanes(initialSwimlaneState));
    dispatch(resetTasks(initialTaskState));
    setIsOpen(false);
  };

  return (
    <Dialog open={isOpen} onClose={handleClose} fullWidth>
      <DialogTitle>Reset All Data</DialogTitle>
      <DialogContent dividers>
        <DialogContentText variant='h5' paragraph>
          <strong>WARNING:</strong> This action will remove all current app
          settings, swimlanes, and tasks!
        </DialogContentText>
        <DialogContentText paragraph>
          <strong>To save current app state:</strong> cancel this dialog and use
          the Export menu option to download everything to a file.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button variant='contained' color='warning' onClick={clearLocalStorage}>
          Reset
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ResetDataDialog;

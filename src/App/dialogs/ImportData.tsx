import {
  ChangeEvent,
  SetStateAction,
  useEffect,
  useRef,
  useState
} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText
} from '@mui/material';

import { useAppDispatch } from '../../storage/hooks';
import { migrate } from '../../storage/migrate';
import { RootState } from '../../storage/store';
import {
  initialState as swimlanesInitialState,
  reset as swimlanesReset
} from '../../Swimlane/slice';
import {
  initialState as tasksInitialState,
  reset as tasksReset
} from '../../Task/slice';
import { CustomDialogTitle as DialogTitle } from '../../theme/CustomDialogTitle';
import { initialState as appInitialState, reset as appReset } from '../slice';

/** Confirmation and warning to import data. */
const ImportDataDialog = ({
  isOpen,
  setIsOpen
}: {
  isOpen: boolean;
  setIsOpen: (value: SetStateAction<boolean>) => void;
}) => {
  const dispatch = useAppDispatch();

  const fileInput = useRef<HTMLInputElement | null>(null);

  const [importedState, setImportedState] = useState<RootState>();

  const handleClose = () => {
    setIsOpen(false);
  };

  /** fire click on invisible input element */
  const handleFileInput = () => {
    if (fileInput.current) {
      fileInput.current.click();
    }
  };

  /** replace local storage with content of json file */
  const importData = async (event: ChangeEvent<HTMLInputElement>) => {
    const {
      target: { files }
    } = event;
    if (files && files.length > 0) {
      try {
        setImportedState(JSON.parse(JSON.parse(await files[0].text())));
      } catch {
        alert('Unexpected file or format.');
        setImportedState(undefined);
        setIsOpen(false);
      }
    }
  };

  // restore imported state
  useEffect(() => {
    if (importedState) {
      const updatedState = migrate(importedState);

      const { app, swimlanes, tasks } = updatedState;

      dispatch(appReset({ ...appInitialState, ...app }));
      dispatch(swimlanesReset({ ...swimlanesInitialState, ...swimlanes }));
      dispatch(tasksReset({ ...tasksInitialState, ...tasks }));
      setImportedState(undefined);
      setIsOpen(false);
    }
  }, [importedState]);

  return (
    <>
      <Dialog open={isOpen} onClose={handleClose} fullWidth>
        <DialogTitle>Import Data</DialogTitle>
        <DialogContent dividers>
          <DialogContentText variant='h5' paragraph>
            <strong>WARNING:</strong> This action will overwrite all current app
            settings, swimlanes, and tasks!
          </DialogContentText>
          <DialogContentText paragraph>
            <strong>To save current app state:</strong> cancel this dialog and
            use the Export menu option to download everything to a file.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button variant='contained' color='warning' onClick={handleFileInput}>
            Import
          </Button>
        </DialogActions>
      </Dialog>

      <input
        id='fileUpload'
        ref={fileInput}
        type='file'
        onChange={importData}
        hidden
      />
    </>
  );
};

export default ImportDataDialog;

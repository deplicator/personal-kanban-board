import { SetStateAction } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  Link,
  List,
  ListItem
} from '@mui/material';

import { CustomDialogTitle as DialogTitle } from '../../theme/CustomDialogTitle';

const AboutDialog = ({
  isOpen,
  setIsOpen
}: {
  isOpen: boolean;
  setIsOpen: (value: SetStateAction<boolean>) => void;
}) => {
  const handleClose = () => {
    setIsOpen(false);
  };

  return (
    <Dialog open={isOpen} onClose={handleClose} fullWidth>
      <DialogTitle>About</DialogTitle>
      <DialogContent dividers>
        <DialogContentText paragraph>
          A personal project built for fun. More details on the{' '}
          <Link href='https://gitlab.com/deplicator/personal-kanban-board'>
            Gitlab project page
          </Link>
          .
        </DialogContentText>
        <DialogContentText paragraph>
          <List>
            <ListItem>
              <Link href='https://gitlab.com/deplicator/personal-kanban-board/-/blob/develop/docs/PLANNING.md'>
                Planned Improvements
              </Link>
            </ListItem>
            <ListItem>
              <Link href='https://gitlab.com/deplicator/personal-kanban-board/-/blob/develop/docs/RELEASES.md'>
                Release Notes
              </Link>
            </ListItem>
            <ListItem>
              <Link
                href={
                  'mailto:james@geekwagon.net?subject=What were you thinking when you made Personal Kanban Board?'
                }
              >
                Contact Me
              </Link>
            </ListItem>
          </List>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Close</Button>
      </DialogActions>
    </Dialog>
  );
};

export default AboutDialog;

import { initialState as appInitialState } from '../App/slice';
import { RootState } from '../storage/store';
import { initialState as swimlaneInitialState } from '../Swimlane/slice';
import { initialState as tasksInitialState } from '../Task/slice';

export const testState0 = {
  app: {
    ...appInitialState,
    boardTitle: 'Test App'
  },
  swimlanes: swimlaneInitialState,
  tasks: tasksInitialState
} as RootState;

export const testState1 = {
  app: {
    ...appInitialState,
    boardTitle: 'Test App'
  },
  swimlanes: {
    ...swimlaneInitialState,
    swimlaneList: [
      {
        ...swimlaneInitialState.swimlaneList[0],
        taskOrder: [0, 2, 4]
      },
      {
        ...swimlaneInitialState.swimlaneList[1],
        taskOrder: [5]
      },
      {
        ...swimlaneInitialState.swimlaneList[2],
        taskOrder: [1]
      }
    ]
  },
  tasks: {
    taskList: [
      {
        id: 0,
        title: 'test task',
        description: 'test description'
      },
      {
        id: 1,
        title: 'test task 1',
        description: 'test description 1'
      },
      {
        id: 2,
        title: 'test task 2',
        description: 'test description 2'
      },
      {
        id: 4,
        title: 'test task 4',
        description: 'test description 4'
      },
      {
        id: 5,
        title: 'test task 5',
        description: 'test description 5'
      }
    ],
    nextTaskId: 6
  }
} as RootState;

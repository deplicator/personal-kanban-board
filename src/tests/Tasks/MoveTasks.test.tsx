import { screen, within } from '@testing-library/react';
import user from '@testing-library/user-event';

import App from '../../App';
import { testState1 } from '../constants';
import { renderWithProviders } from '../utils';

describe('Move Task', () => {
  it('should reorder tasks within a swimlane', async () => {
    const { store } = renderWithProviders(<App />, {
      preloadedState: testState1
    });

    expect(store.getState().swimlanes.swimlaneList[0].taskOrder).toStrictEqual([
      0, 2, 4
    ]);

    // Move tasks around
    user.click(
      within(await screen.findByTestId('task-2')).getByRole('button', {
        name: 'move-up'
      })
    );
    user.click(
      within(screen.getByTestId('task-0')).getByRole('button', {
        name: 'move-down'
      })
    );

    expect(store.getState().swimlanes.swimlaneList[0].taskOrder).toStrictEqual([
      2, 4, 0
    ]);
  });

  it('should move task between swimlanes', async () => {
    renderWithProviders(<App />, {
      preloadedState: testState1
    });

    const waitingSwimlane = await screen.findByTestId('swimlane-0');
    const inProgressSwimlane = await screen.findByTestId('swimlane-1');
    const completeSwimlane = await screen.findByTestId('swimlane-2');

    // Verify task is in Waiting
    expect(
      await within(waitingSwimlane).findByTestId('task-4')
    ).toBeInTheDocument();

    // Move task forward
    user.click(
      within(screen.getByTestId('task-4')).getByRole('button', {
        name: 'move-forward'
      })
    );

    // Verify task is in In Progress
    expect(
      within(inProgressSwimlane).getByTestId('task-4')
    ).toBeInTheDocument();

    // Move task forward
    user.click(
      within(screen.getByTestId('task-4')).getByRole('button', {
        name: 'move-forward'
      })
    );

    // Verify task is in Complete
    expect(within(completeSwimlane).getByTestId('task-4')).toBeInTheDocument();

    // Move task back
    user.click(
      within(screen.getByTestId('task-4')).getByRole('button', {
        name: 'move-back'
      })
    );

    // Verify task is in In Progress
    expect(
      within(inProgressSwimlane).getByTestId('task-4')
    ).toBeInTheDocument();

    // Move task back
    user.click(
      within(screen.getByTestId('task-4')).getByRole('button', {
        name: 'move-back'
      })
    );

    // Verify task is in Waiting
    expect(within(waitingSwimlane).getByTestId('task-4')).toBeInTheDocument();
  });
});

import { screen, waitFor, within } from '@testing-library/react';
import user from '@testing-library/user-event';

import App from '../../App';
import { testState0 } from '../constants';
import { renderWithProviders } from '../utils';

describe('Create Task', () => {
  beforeEach(() => {
    renderWithProviders(<App />, {
      preloadedState: testState0
    });
  });

  it('should have new task button in Waiting swimlane', async () => {
    await waitFor(() => {
      expect(
        within(screen.getByTestId('swimlane-0')).getByLabelText(/add-task/i)
      ).toBeInTheDocument();
    });
  });

  it('should show new task in edit mode on button click', async () => {
    await waitFor(() => {
      user.click(
        within(screen.getByTestId('swimlane-0')).getByLabelText(/add-task/i)
      );
    });

    // Elements for task in edit mode
    const titleTextField = await screen.findByRole('textbox', {
      name: /title/i
    });
    const descriptionTextField = await screen.findByRole('textbox', {
      name: /description/i
    });
    const saveButton = await screen.findByRole('button', { name: /save/i });

    expect(titleTextField).toBeInTheDocument();
    expect(descriptionTextField).toBeInTheDocument();
    expect(saveButton).toBeInTheDocument();
    expect(saveButton).toHaveAttribute('disabled'); // save button disabled until title has text

    user.keyboard('test task');

    expect(saveButton).not.toHaveAttribute('disabled');

    user.tab();
    user.keyboard('test description');

    user.click(saveButton);

    // New task should show in Waiting swimlane
    const waitingSwimlane = screen.getByTestId('swimlane-0');

    expect(
      within(waitingSwimlane).getByRole('heading', { name: /test task/i })
    ).toBeInTheDocument();

    expect(
      within(waitingSwimlane).getByText(/test description/i)
    ).toBeInTheDocument();
  });
});

import { screen, within } from '@testing-library/react';
import user from '@testing-library/user-event';

import App from '../../App';
import { testState1 } from '../constants';
import { renderWithProviders } from '../utils';

describe('Update Task', () => {
  beforeEach(() => {
    renderWithProviders(<App />, {
      preloadedState: testState1
    });
  });

  it('should edit task title and description', async () => {
    const taskElement = await screen.findByTestId('task-5');

    const editButton = within(taskElement).getByRole('button', {
      name: 'edit-task'
    });

    // Verify current task title and description
    expect(
      screen.getByRole('heading', { name: /test task 5/i })
    ).toBeInTheDocument();
    expect(screen.getByText(/test description 5/i)).toBeInTheDocument();

    // Edit task
    user.click(editButton);

    const titleTextField = screen.getByRole('textbox', {
      name: /title/i
    });
    const saveButton = screen.getByRole('button', { name: /save-task/i });

    user.dblClick(titleTextField);
    user.keyboard('edited task title');
    user.tab();
    user.keyboard('edited task description');
    user.click(saveButton);

    // Verify edit saved
    expect(
      screen.getByRole('heading', { name: /edited task title/i })
    ).toBeInTheDocument();
    expect(screen.getByText(/edited task description/i)).toBeInTheDocument();
  });

  it('should cancel edit tasks', async () => {
    const taskElement = await screen.findByTestId('task-1');

    // Verify current task title and description
    expect(
      screen.getByRole('heading', { name: /test task 1/i })
    ).toBeInTheDocument();
    expect(screen.getByText(/test description 1/i)).toBeInTheDocument();

    // Edit task
    user.click(
      within(taskElement).getByRole('button', {
        name: 'edit-task'
      })
    );

    const titleTextField = screen.getByRole('textbox', {
      name: /title/i
    });
    const cancelButton = screen.getByRole('button', { name: /cancel-task/i });

    user.dblClick(titleTextField);
    user.keyboard('edited task title');
    user.tab();
    user.keyboard('edited task description');
    user.click(cancelButton);

    // Verify edit did not save
    expect(
      screen.getByRole('heading', { name: /test task 1/i })
    ).toBeInTheDocument();
    expect(screen.getByText(/test description 1/i)).toBeInTheDocument();

    // Verify text fields reset for next edit
    user.click(
      within(taskElement).getByRole('button', {
        name: 'edit-task'
      })
    );
    expect(screen.getByRole('textbox', { name: /title/i })).toHaveValue(
      'test task 1'
    );
    expect(screen.getByRole('textbox', { name: /description/i })).toHaveValue(
      'test description 1'
    );
  });
});

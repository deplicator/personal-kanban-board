import { screen, within } from '@testing-library/react';
import user from '@testing-library/user-event';

import App from '../../App';
import { testState1 } from '../constants';
import { renderWithProviders } from '../utils';

describe('Update Task', () => {
  beforeEach(() => {
    renderWithProviders(<App />, {
      preloadedState: testState1
    });
  });

  it('should delete tasks', async () => {
    const taskElement = await screen.findByTestId('task-2');

    const editButton = within(taskElement).getByRole('button', {
      name: 'edit-task'
    });

    // Verify current task title and description
    expect(
      screen.getByRole('heading', { name: /test task 2/i })
    ).toBeInTheDocument();
    expect(screen.getByText(/test description 2/i)).toBeInTheDocument();

    // Delete task
    user.click(editButton);

    const deleteButton = screen.getByRole('button', { name: /archive-task/i });

    user.click(deleteButton);

    // Verify task has been removed
    expect(
      screen.queryByRole('heading', { name: /test task 2/i })
    ).not.toBeInTheDocument();
    expect(screen.queryByText(/task description 2/i)).not.toBeInTheDocument();
  });
});

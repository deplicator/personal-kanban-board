import { createTheme, Theme } from '@mui/material';

const baseTheme = {
  typography: {
    h1: {
      fontSize: 24,
      fontWeight: 700
    },
    h2: {
      fontSize: 22,
      fontWeight: 700
    },
    h3: {
      fontSize: 20,
      fontWeight: 400
    }
  },
  components: {
    MuiButton: {
      defaultProps: {
        variant: 'outlined'
      }
    },
    MuiCard: {
      defaultProps: {
        variant: 'outlined'
      }
    },
    MuiMenu: {
      styleOverrides: {
        list: {
          minWidth: 250
        }
      }
    }
  }
} as Theme;

/** MUI defaults for light mode, could arguably have been called blueTheme. */
export const lightTheme = createTheme(
  {
    palette: { mode: 'light' }
  },
  baseTheme
);

/** MUI defaults for dark mode. */
export const darkTheme = createTheme(
  {
    palette: { mode: 'dark' }
  },
  baseTheme
);

/** Uses complementary color from MUI default primary palette. Changes warning
 * to error color because it is a similar orange. */
export const orangeTheme = createTheme(
  {
    palette: {
      mode: 'light',
      primary: {
        main: '#D27519',
        light: '#F59242',
        dark: '#C07015'
      },
      secondary: {
        main: '#3BB027',
        light: '#76C868',
        dark: '#46A21F'
      },
      warning: {
        main: '#d32f2f',
        light: '#ef5350',
        dark: '#c62828'
      }
    }
  },
  baseTheme
);

/** Uses first tetradic color from MUI default primary palette, rotating clockwise. */
export const pinkTheme = createTheme(
  {
    palette: {
      mode: 'light',
      primary: {
        main: '#D119D2',
        light: '#EC42F5',
        dark: '#C015BA'
      },
      secondary: {
        main: '#B05727',
        light: '#C88A68',
        dark: '#A2391F'
      }
    }
  },
  baseTheme
);

/** Uses first tetradic color from MUI default primary palette, rotating counter-clockwise. */
export const greenTheme = createTheme(
  {
    palette: {
      mode: 'light',
      primary: {
        main: '#1AD219',
        light: '#4BF542',
        dark: '#15C01B'
      },
      secondary: {
        main: '#2780B0',
        light: '#68A6C8',
        dark: '#1F88A2'
      }
    }
  },
  baseTheme
);

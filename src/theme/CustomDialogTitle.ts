import { DialogTitle, DialogTitleProps, styled } from '@mui/material';

/** Uses theme primary dark color for Dialog Title background */
export const CustomDialogTitle = styled(DialogTitle)<DialogTitleProps>(
  ({ theme }) => ({
    color: theme.palette.primary.contrastText,
    backgroundColor: theme.palette.primary.dark,
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  })
);

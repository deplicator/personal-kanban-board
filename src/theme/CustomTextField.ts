import { styled, TextField, TextFieldProps } from '@mui/material';

/** For editing app title (h1) */
export const CustomTextField = styled(TextField)<TextFieldProps>(
  ({ theme }) => ({
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: theme.palette.primary.contrastText
      },
      '&:hover fieldset': {
        borderColor: theme.palette.primary.contrastText
      },
      '&.Mui-focused fieldset': {
        borderColor: theme.palette.primary.contrastText
      }
    },
    '& input': {
      color: theme.palette.primary.contrastText,
      backgroundColor: theme.palette.primary.light,
      fontSize: theme.typography.h1.fontSize,
      fontWeight: theme.typography.h1.fontWeight,
      letterSpacing: theme.typography.h1.letterSpacing,
      padding: '0 0 0 8px'
    }
  })
);

import { Theme } from '@mui/material';

import {
  darkTheme,
  greenTheme,
  lightTheme,
  orangeTheme,
  pinkTheme
} from './theme';

/** Short hand name for themes, used for storage and access */
export type ThemeNames = 'light' | 'dark' | 'orange' | 'pink' | 'green';

// https://dev.to/k_penguin_sato/use-lookup-tables-for-cleaning-up-your-js-ts-code-9gk
const themeLookup: Record<ThemeNames, Theme> = {
  light: lightTheme,
  dark: darkTheme,
  orange: orangeTheme,
  pink: pinkTheme,
  green: greenTheme
};

/** Accepts theme name, returns theme */
export const themeMapper = (themeName: ThemeNames): Theme =>
  themeLookup[themeName] || lightTheme;

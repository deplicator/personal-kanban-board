import { migrate } from './migrate';
import { RootState } from './store';

// https://stackoverflow.com/a/61943772
/** Pass in Redux store's state to save it to the user's browser local storage */
export const saveState = (state: RootState) => {
  try {
    localStorage.setItem('state', JSON.stringify(state));
  } catch {
    console.error('saveState error');
  }
};

/** Loads state, returns object that can used in preloadedState of configureStore */
export const loadState = () => {
  try {
    const serializedState = localStorage.getItem('state');
    if (serializedState === null) {
      return undefined;
    }

    const parsedState = JSON.parse(serializedState) as RootState;

    return migrate(parsedState);
  } catch (error) {
    console.error('loadState error');
    return undefined;
  }
};

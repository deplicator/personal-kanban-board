import { initialState } from '../App/slice';

import { saveState } from './localStorage';
import { RootState } from './store';

/** Returns true if current version is older than checked version. */
const isVersionOlder = (current: string, checked: string) => {
  if (!current) {
    return true;
  }

  // https://stackoverflow.com/a/55466325
  if (
    current.localeCompare(checked, undefined, {
      numeric: true,
      sensitivity: 'base'
    }) < 0
  ) {
    return true;
  }

  return false;
};

/** Update state being passed in to current version. */
export const migrate = (data: RootState) => {
  // Catch the case when migration is before when there were versions stored.
  // Not really handled, no way to know what state was at the incoming version.
  if (isVersionOlder(data.app.version, '1.3.0')) {
    alert(
      'Local storage data is to old to be compatible with current version of Personal Kanban Board. \n\n' +
        'Old tasks may still be stored in the browsers local storage and can be exported through the data menu even though they will not show on the board.\n\n' +
        'To prevent this alert from popping up again, reset data with the menu.'
    );
    return data;
  }

  const oldVersion = data.app.version;
  let updateFlag = false;

  // Migrate from 1.3.x to 1.4.0
  if (isVersionOlder(data.app.version, '1.4.0')) {
    updateFlag = true;
    data.app.version = '1.4.0';

    // Locks added to app state
    data.app.locks = initialState.locks;

    // Swimlanes updated to include an order
    data.swimlanes.swimlaneOrder = [0, 1, 2];
    data.swimlanes.nextSwimlaneId = 3;

    // newTaskButton boolean name change to addTaskButton
    data.swimlanes.swimlaneList = data.swimlanes.swimlaneList.map(
      (swimlane) => ({
        ...swimlane,
        addTaskButton: swimlane.newTaskButton || false
      })
    );
  }

  // Migrate from 1.4.0 to 1.4.1
  if (isVersionOlder(data.app.version, '1.4.1')) {
    updateFlag = true;
    data.app.version = '1.4.1';
  }

  if (updateFlag) {
    saveState(data);
    alert(
      `Successful local storage update!\n
    Old Version: ${oldVersion}
    New Version: ${data.app.version}`
    );
  }

  return data;
};

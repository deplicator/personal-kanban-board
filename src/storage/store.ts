import {
  combineReducers,
  configureStore,
  PreloadedState
} from '@reduxjs/toolkit';

import appReducer from '../App/slice';
import swimlaneReducer from '../Swimlane/slice';
import tasksReducer from '../Task/slice';

import { loadState } from './localStorage';

const rootReducer = combineReducers({
  app: appReducer,
  swimlanes: swimlaneReducer,
  tasks: tasksReducer
});

export const setupStore = (preloadedState?: PreloadedState<RootState>) => {
  return configureStore({
    reducer: rootReducer,
    preloadedState: preloadedState ? preloadedState : loadState()
  });
};

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof setupStore>;
export type AppDispatch = AppStore['dispatch'];

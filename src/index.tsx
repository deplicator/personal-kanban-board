import { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';

import App from './App';
import reportWebVitals from './reportWebVitals';
import { saveState } from './storage/localStorage';
import { setupStore } from './storage/store';
import throttle from 'lodash.throttle';

const store = setupStore();

// Subscribe to state changes, saving the store's state to the browser's local storage.
store.subscribe(throttle(() => saveState(store.getState()), 1000));

const root = createRoot(document.getElementById('root') as HTMLElement);

root.render(
  <StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
